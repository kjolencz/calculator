package edu.towson.cosc431.KevinOlencz.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var curText = "";
        var lengthAtSymbol = 0;
        var operator = ""
        one.setOnClickListener(){
            val temp  = "1"
            curText += temp
            result.text = curText
        }

        two.setOnClickListener(){
            val temp  = "2"
            curText += temp
            result.text = curText
        }

        three.setOnClickListener(){
            val temp  = "3"
            curText += temp
            result.text = curText
        }

        four.setOnClickListener(){
            val temp  = "4"
            curText += temp
            result.text = curText
        }

        five.setOnClickListener(){
            val temp  = "5"
            curText += temp
            result.text = curText
        }

        six.setOnClickListener(){
            val temp  = "6"
            curText += temp
            result.text = curText
        }


        seven.setOnClickListener(){
            val temp  = "7"
            curText += temp
            result.text = curText
        }

        eight.setOnClickListener(){
            val temp  = "8"
            curText += temp
            result.text = curText
        }

        nine.setOnClickListener(){
            val temp  = "9"
            curText += temp
            result.text = curText
        }

        zero.setOnClickListener(){
            val temp  = "0"
            curText += temp
            result.text = curText
        }

        add.setOnClickListener(){
            val temp  = "+"
            lengthAtSymbol = curText.length
            curText += temp
            operator = "+"
            result.text = curText
        }

        subtract.setOnClickListener(){
            val temp  = "-"
            lengthAtSymbol = curText.length
            curText += temp
            operator = "-"
            result.text = curText
        }

        multiply.setOnClickListener(){
            val temp  = "*"
            lengthAtSymbol = curText.length
            curText += temp
            operator = "*"
            result.text = curText
        }

        divide.setOnClickListener(){
            val temp  = "/"
            lengthAtSymbol = curText.length
            curText += temp
            operator = "/"
            result.text = curText
        }

        decimal.setOnClickListener(){
            val temp  = "."
            curText += temp
            result.text = curText
        }

        clear.setOnClickListener(){
            curText = ""
            result.text = ""
        }

        equals.setOnClickListener(){
            var num1 = curText.substring(0,lengthAtSymbol)
            var num2 = curText.substring(lengthAtSymbol+1,curText.length)
            var n1 = num1.toBigInteger()
            var n2 = num2.toBigInteger()

            if(operator.equals("+")){
                val total = n1+n2
                curText = total.toString()
                result.text = curText
            }

            if(operator.equals("-")){
                val total = n1-n2
                curText = total.toString()
                result.text = curText
            }

            if(operator.equals("*")){
                val total = n1*n2
                curText = total.toString()
                result.text = curText
            }

            if(operator.equals("/")){
                var number1 = num1.toDouble()
                var number2 = num2.toDouble()
                if(number2 != 0.0) {
                    var total = number1 / number2
                    curText = total.toString()
                    result.text = curText
                }else{
                    result.text = "ERR DIVIDE BY ZERO"
                }
            }

        }


    }


}
